<?php

namespace App\Http\Controllers;

use App\Helper\Enkrip;
use Illuminate\Http\Request;

class pemasukanController extends Controller
{
    public function index()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();

        $pemasukan = $client->request('GET', "http://127.0.0.1:8080/api/pemasukan");
        $datapemasukan = json_decode($pemasukan->getBody(), true);
        

        $x = $dekrip->dekrip($datapemasukan);
        $datax = json_decode($x,true);

        $transaksi = $client->request('GET', "http://127.0.0.1:8080/api/transaksi");
        $data = json_decode($transaksi->getBody(), true);

        $detailpemasukan = $datax['detailpemasukan'];
        $datapemasukan = $datax['datapemasukan'];

        if($data)
        {
            $total_pemasukan = $data['totalpemasukan'];
        }
        else
        {
            $total_pemasukan = "";
        }

        return view('datapemasukan.datapemasukan')->with(compact('datapemasukan', 'detailpemasukan', 'total_pemasukan'));
    }

    public function delete(request $request)
    {
        $kdtr_pemasukan = $request->kdtr_pemasukan;

        $client = new \GuzzleHttp\Client();
        $pemasukan = $client->request('DELETE', "http://127.0.0.1:8080/api/pemasukan/hapus/".$kdtr_pemasukan,[
            'form_params' => [
                'kdtr_pemasukan' => $kdtr_pemasukan
            ]
        ]);
        $delpemasukan = json_decode($pemasukan->getBody(), true);
        $status = $delpemasukan['status'];
        $message = $delpemasukan['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function tambahform()
    {
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('GET', "http://127.0.0.1:8080/api/kategori");
        $datakategori = json_decode($kategori->getBody(), true);

        $barang = $client->request('GET', "http://127.0.0.1:8080/api/barang");
        $databarang = json_decode($barang->getBody(), true);

        return view('datapemasukan.tambahpemasukan')->with(compact('datakategori', 'databarang'));
    }

    public function tambah(request $request)
    {
        $kdtr_pemasukan = $request->kdtr_pemasukan;
        $tgl = $request->tgl;
        $kd_barang = $request->kd_barang;
        $kd_kategori = $request->kd_kategori;
        $qty = $request->qty;
        $harga = $request->harga;
        $subtotal = $request->subtotal;
        $user = 'login';
        $client = new \GuzzleHttp\Client();

        $pemasukan = $client->request('POST', "http://127.0.0.1:8080/api/pemasukan/tambah/",[
            'form_params' => [
                'kdtr_pemasukan' => $kdtr_pemasukan,
                'tgl' => $tgl,
                'kd_barang' => $kd_barang,
                'kd_kategori' => $kd_kategori,
                'qty' => $qty,
                'harga' => $harga,
                'subtotal' => $subtotal,
                'user' => $user
            ]
        ]);
        $datapemasukan = json_decode($pemasukan->getBody(), true);
        $status = $datapemasukan['status'];
        $message = $datapemasukan['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }
}
