<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function index()
    {
        $client = new \GuzzleHttp\Client();
        $dashboard = $client->request('GET', "http://127.0.0.1:8080/api/");
        $data = json_decode($dashboard->getBody(), true);

        if($data)
        {
            $pengeluaran = $data['pengeluaran'];
            $pemasukan = $data['pemasukan'];
            $balance = $data['balance'];
            $stok = $data['stok'];
            $kategori = $data['kategori'];
            $suplier = $data['suplier'];
        }

        else
        {
            $pengeluaran = "";
            $pemasukan = "";
            $balance = "";
            $stok = "";
            $kategori = "";
            $suplier = "";
        }

        return view('dashboard')->with(compact('pengeluaran', 'pemasukan','balance','stok','kategori','suplier'));
    }
}
