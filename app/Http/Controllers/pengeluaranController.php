<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Enkrip;

class pengeluaranController extends Controller
{
    public function index()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();

        $pengeluaran = $client->request('GET', "http://127.0.0.1:8080/api/pengeluaran");
        $datapengeluaran = json_decode($pengeluaran->getBody(), true);

        $x = $dekrip->dekrip($datapengeluaran);
        $datax = json_decode($x,true);
        
        $transaksi = $client->request('GET', "http://127.0.0.1:8080/api/transaksi");
        $data = json_decode($transaksi->getBody(), true);
        
        $detailpengeluaran = $datax['detailpengeluaran'];
        $datapengeluaran = $datax['datapengeluaran'];

        if($data)
        {
            $total_pengeluaran = $data['totalpengeluaran'];
            $total_pemasukan = $data['totalpemasukan'];
        }
        else
        {
            $total_pengeluaran = "";
            $total_pemasukan = "";
        }
        

        return view('datapengeluaran.datapengeluaran')->with(compact('detailpengeluaran','datapengeluaran','total_pengeluaran','total_pemasukan'));
    }

    public function delete(request $request)
    {
        $kdtr_pengeluaran = $request->kdtr_pengeluaran;

        $client = new \GuzzleHttp\Client();
        $pengeluaran = $client->request('DELETE', "http://127.0.0.1:8080/api/pengeluaran/hapus/".$kdtr_pengeluaran,[
            'form_params' => [
                'kdtr_pengeluaran' => $kdtr_pengeluaran
            ]
        ]);
        $delpengeluaran = json_decode($pengeluaran->getBody(), true);
        $status = $delpengeluaran['status'];
        $message = $delpengeluaran['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function tambahform()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('GET', "http://127.0.0.1:8080/api/kategori");
        $x = json_decode($kategori->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datakategori = json_decode($datax,true);

        $barang = $client->request('GET', "http://127.0.0.1:8080/api/barang");
        $x2 = json_decode($barang->getBody(), true);
        $datax = $dekrip->dekrip($x2);
        // dd($datax);
        $databarang = json_decode($datax,true);

        return view('datapengeluaran.tambahpengeluaran')->with(compact('datakategori', 'databarang'));
    }

    public function tambah(request $request)
    {
        $kdtr_pengeluaran = $request->kdtr_pengeluaran;
        $tgl = $request->tgl;
        $kd_barang = $request->kd_barang;
        $kd_kategori = $request->kd_kategori;
        $qty = $request->qty;
        $harga = $request->harga;
        $subtotal = $request->subtotal;
        $user = 'login';
        $client = new \GuzzleHttp\Client();

        $pengeluaran = $client->request('POST', "http://127.0.0.1:8080/api/pengeluaran/tambah/",[
            'form_params' => [
                'kdtr_pengeluaran' => $kdtr_pengeluaran,
                'tgl' => $tgl,
                'kd_barang' => $kd_barang,
                'kd_kategori' => $kd_kategori,
                'qty' => $qty,
                'harga' => $harga,
                'subtotal' => $subtotal,
                'user' => $user
            ]
        ]);
        $datapengeluaran = json_decode($pengeluaran->getBody(), true);
        $status = $datapengeluaran['status'];
        $message = $datapengeluaran['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }
}
