<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Enkrip;

class barangController extends Controller
{
    public function index()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();

        $barang = $client->request('GET', "http://127.0.0.1:8080/api/barang");
        $x = json_decode($barang->getBody(), true);

        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $databarang = json_decode($datax,true);

        return view('databarang.databarang')->with(compact('databarang'));
    }

    public function delete(request $request)
    {
        $kd_barang = $request->kd_barang;
        $client = new \GuzzleHttp\Client();

        $barang = $client->request('DELETE', "http://127.0.0.1:8080/api/barang/hapus/".$kd_barang,[
            'form_params' => [
                'kd_barang' => $kd_barang
            ]
        ]);
        $databarang = json_decode($barang->getBody(), true);
        $status = $databarang['status'];
        $message = $databarang['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function ubah(request $request)
    {
        $dekrip = new Enkrip();
        $kd_barang = $request->kd_barang;
        $nama_barang = $request->nama_barang;
        $kd_kategori = $request->kd_kategori;
        $stok = $request->stok;
        $harga_beli = $request->harga_beli;
        $harga_jual = $request->harga_jual;
        $kd_suplier = $request->kd_suplier;

        $client = new \GuzzleHttp\Client();
        $barang = $client->request('GET', "http://127.0.0.1:8080/api/barang/ubah/".$kd_barang,[
            'form_params' => [
                'kd_barang' => $kd_barang,
                'nama_barang' => $nama_barang,
                'kd_kategori' => $kd_kategori,
                'stok' => $stok,
                'harga_beli' => $harga_beli,
                'harga_jual' => $harga_jual,
                'kd_suplier' => $kd_suplier
            ]
        ]);
        $x = json_decode($barang->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $databarang = json_decode($datax,true);
        $data = [
            "data" => $databarang
        ];
        // dd($datakategori);

        return view('databarang.ubahbarang')->with(compact('databarang'));
    }

    public function update(request $request)
    {
        $kd_barang = $request->kd_barang;
        $nama_barang = $request->nama_barang;
        $kd_kategori = $request->kd_kategori;
        $stok = $request->stok;
        $harga_beli = $request->harga_beli;
        $harga_jual = $request->harga_jual;
        $kd_suplier = $request->kd_suplier;

        $client = new \GuzzleHttp\Client();

        $barang = $client->request('PUT', "http://127.0.0.1:8080/api/barang/update/".$kd_barang,[
            'form_params' => [
                'kd_barang' => $kd_barang,
                'nama_barang' => $nama_barang,
                'kd_kategori' => $kd_kategori,
                'stok' => $stok,
                'harga_beli' => $harga_beli,
                'harga_jual' => $harga_jual,
                'kd_suplier' => $kd_suplier
            ]
        ]);
        $databarang = json_decode($barang->getBody(), true);
        $status = $databarang['status'];
        $message = $databarang['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function tambahform()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('GET', "http://127.0.0.1:8080/api/kategori");
        $x = json_decode($kategori->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datakategori = json_decode($datax,true);

        $suplier = $client->request('GET', "http://127.0.0.1:8080/api/suplier");
        $x2 = json_decode($suplier->getBody(), true);
        $datax = $dekrip->dekrip($x2);
        // dd($datax);
        $datasuplier = json_decode($datax,true);

        return view('databarang.tambahbarang')->with(compact('datakategori', 'datasuplier'));
    }
    public function tambah(request $request)
    {
        $kd_barang = $request->kd_barang;
        $nama_barang = $request->nama_barang;
        $kd_kategori = $request->kd_kategori;
        $stok = $request->stok;
        $harga_beli = $request->harga_beli;
        $harga_jual = $request->harga_jual;
        $kd_suplier = $request->kd_suplier;


        $client = new \GuzzleHttp\Client();

        $barang = $client->request('POST', "http://127.0.0.1:8080/api/barang/tambah/",[
            'form_params' => [
                'kd_barang' => $kd_barang,
                'nama_barang' => $nama_barang,
                'kd_barang' => $kd_barang,
                'kd_kategori' => $kd_kategori,
                'stok' => $stok,
                'harga_beli' => $harga_beli,
                'harga_jual' => $harga_jual,
                'kd_suplier' => $kd_suplier
            ]
        ]);
        $databarang = json_decode($barang->getBody(), true);
        $status = $databarang['status'];
        $message = $databarang['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }
}
