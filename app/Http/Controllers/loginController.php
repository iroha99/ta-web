<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class loginController extends Controller
{
    public function login(request $request)
    {
        $user = $request->user;
        $password = $request->password;
        $client = new \GuzzleHttp\Client();

        $login = $client->request('POST', "http://127.0.0.1:8080/api/login/",[
            'form_params' => [
                'user' => $user,
                'password' => $password,
                'type' => 'web'
            ]
        ]);
        $loginproc = json_decode($login->getBody(), true);
        $status = $loginproc['status'];
        $message = $loginproc['message'];

        if($status == "success")
        {
            if($loginproc['role'] == 0)
            {
                $detrole = "Admin";
            }
            else
            {
                $detrole = "Staff";
            }

            Session::put('user', $user);
            Session::put('token', $loginproc['token']);
            Session::put('detrole', $detrole);
            Session::put('role', $loginproc['role']);
            return redirect('/');
        }

        return redirect()->back()->with('status', $status)->with('message', $message);
        
    }

    public function logout(request $request)
    {
        $user = Session::get('user');
        if($user != "")
        {
        $client = new \GuzzleHttp\Client();

        $logout = $client->request('POST', "http://127.0.0.1:8080/api/logout/",[
            'form_params' => [
                'user' => $user
            ]
        ]);
        }

        $request->session()->flush();
        return redirect("/login");
    }
}
