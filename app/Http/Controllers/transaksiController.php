<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class transaksiController extends Controller
{
    public function index()
    {
        $client = new \GuzzleHttp\Client();

        $transaksi = $client->request('GET', "http://127.0.0.1:8080/api/transaksi");
        $data = json_decode($transaksi->getBody(), true);

        if($data)
        {
            $transaksi = $data['datatransaksi'];
            $total_pengeluaran = $data['totalpengeluaran'];
            $total_pemasukan = $data['totalpemasukan'];
        }
        else
        {
            $transaksi = "";
            $total_pengeluaran = "";
            $total_pemasukan = "";
        }

        return view('semuatransaksi.transaksi')->with(compact('transaksi', 'total_pengeluaran', 'total_pemasukan'));
    }
}
