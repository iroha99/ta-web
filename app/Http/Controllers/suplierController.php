<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Enkrip;

class suplierController extends Controller
{
    public function index()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();
        // // post ke endpoint yang dituju dengan ngirim variabel
        $suplier = $client->request('GET', "http://127.0.0.1:8080/api/suplier");
        $x = json_decode($suplier->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datasuplier = json_decode($datax,true);

        return view('datasuplier.datasuplier')->with(compact('datasuplier'));
    }

    public function delete(request $request)
    {
        $kd_suplier = $request->kd_suplier;

        $client = new \GuzzleHttp\Client();
        $suplier = $client->request('DELETE', "http://127.0.0.1:8080/api/suplier/hapus/".$kd_suplier,[
            'form_params' => [
                'kd_suplier' => $kd_suplier
            ]
        ]);
        $delsuplier = json_decode($suplier->getBody(), true);
        $status = $delsuplier['status'];
        $message = $delsuplier['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function ubah(request $request)
    {
        $dekrip = new Enkrip();
        $kd_suplier = $request->kd_suplier;
        $nama_suplier = $request->nama_suplier;
        $nomor_tlp = $request->nomor_tlp;
        $alamat_suplier = $request->alamat_suplier;

        $client = new \GuzzleHttp\Client();
        $suplier = $client->request('GET', "http://127.0.0.1:8080/api/suplier/ubah/".$kd_suplier,[
            'form_params' => [
                'kd_suplier' => $kd_suplier,
                'nama_suplier' => $nama_suplier,
                'nomor_tlp' => $nomor_tlp,
                'alamat_suplier' => $alamat_suplier
            ]
        ]);
        $x = json_decode($suplier->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datasuplier = json_decode($datax,true);
        $data = [
            "data" => $datasuplier
        ];
        // dd($datakategori);

        return view('datasuplier.ubahsuplier')->with(compact('datasuplier'));
    }

    public function update(request $request)
    {
        $kd_suplier = $request->kd_suplier;
        $nama_suplier = $request->nama_suplier;
        $nomor_tlp = $request->nomor_tlp;
        $alamat_suplier = $request->alamat_suplier;
        $client = new \GuzzleHttp\Client();

        $suplier = $client->request('PUT', "http://127.0.0.1:8080/api/suplier/update/".$kd_suplier,[
            'form_params' => [
                'kd_suplier' => $kd_suplier,
                'nama_suplier' => $nama_suplier,
                'nomor_tlp'=>$nomor_tlp,
                'alamat_suplier'=>$alamat_suplier
            ]
        ]);
        $datasuplier = json_decode($suplier->getBody(), true);
        $status = $datasuplier['status'];
        $message = $datasuplier['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function tambah(request $request)
    {
        $kd_suplier = $request->kd_suplier;
        $nama_suplier = $request->nama_suplier;
        $nomor_tlp = $request->nomor_tlp;
        $alamat_suplier = $request->alamat_suplier;
        $client = new \GuzzleHttp\Client();

        $suplier = $client->request('POST', "http://127.0.0.1:8080/api/suplier/tambah/",[
            'form_params' => [
                'kd_suplier' => $kd_suplier,
                'nama_suplier' => $nama_suplier,
                'nomor_tlp' => $nomor_tlp,
                'alamat_suplier' => $alamat_suplier
            ]
        ]);
        $datasuplier = json_decode($suplier->getBody(), true);
        $status = $datasuplier['status'];
        $message = $datasuplier['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }
}
