<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Enkrip;

class kategoriController extends Controller
{
    public function index()
    {
        $dekrip = new Enkrip();
        $client = new \GuzzleHttp\Client();
        // // post ke endpoint yang dituju dengan ngirim variabel
        $kategori = $client->request('GET', "http://127.0.0.1:8080/api/kategori");
        $x = json_decode($kategori->getBody(), true);

        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datakategori = json_decode($datax,true);

        return view('datakategori.datakategori')->with(compact('datakategori'));
    }

    public function delete(request $request)
    {
        $kd_kategori = $request->kd_kategori;
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('DELETE', "http://127.0.0.1:8080/api/kategori/hapus/".$kd_kategori,[
            'form_params' => [
                'kd_kategori' => $kd_kategori
            ]
        ]);
        $datakategori = json_decode($kategori->getBody(), true);
        $status = $datakategori['status'];
        $message = $datakategori['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    public function ubah(request $request)
    {
        $dekrip = new Enkrip();
        $kd_kategori = $request->kd_kategori;
        $nama_kategori = $request->nama_kategori;
        $client = new \GuzzleHttp\Client();
        $kategori = $client->request('GET', "http://127.0.0.1:8080/api/kategori/ubah/".$kd_kategori,[
            'form_params' => [
                'kd_kategori' => $kd_kategori,
                'nama_kategori' => $nama_kategori
            ]
        ]);
        $x = json_decode($kategori->getBody(), true);
        $datax = $dekrip->dekrip($x);
        // dd($datax);
        $datakategori = json_decode($datax,true);
        $data = [
            "data" => $datakategori
        ];
        // dd($datakategori);

        return view('datakategori.ubahkategori')->with(compact('datakategori'));
    }

    public function update(request $request)
    {
        $kd_kategori = $request->kd_kategori;
        $nama_kategori = $request->nama_kategori;
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('PUT', "http://127.0.0.1:8080/api/kategori/update/".$kd_kategori,[
            'form_params' => [
                'kd_kategori' => $kd_kategori,
                'nama_kategori' => $nama_kategori
            ]
        ]);
        $datakategori = json_decode($kategori->getBody(), true);
        $status = $datakategori['status'];
        $message = $datakategori['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
        
    }

    public function tambah(request $request)
    {
        $kd_kategori = $request->kd_kategori;
        $nama_kategori = $request->nama_kategori;
        $client = new \GuzzleHttp\Client();

        $kategori = $client->request('POST', "http://127.0.0.1:8080/api/kategori/tambah/",[
            'form_params' => [
                'kd_kategori' => $kd_kategori,
                'nama_kategori' => $nama_kategori
            ]
        ]);
        $datakategori = json_decode($kategori->getBody(), true);
        $status = $datakategori['status'];
        $message = $datakategori['message'];

        return redirect()->back()->with('status', $status)->with('message', $message);
    }

    
}
