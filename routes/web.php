<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/logout', 'loginController@logout');
///////// Login
Route::group(['middleware' => 'Session_In'], function() {
    Route::get('/login', function () {
        return view('login');
    });
    Route::post('/logins', 'loginController@login');
});

Route::group(['middleware' => 'session_out'], function() {

        ///////// dashboard
    Route::get('/', 'dashboardController@index');

    ////////// Data Barang
    Route::prefix('barang')->group(function() 
    {
        Route::get('/', 'barangController@index');
        Route::get('/hapus/{kd_barang}', 'barangController@delete');
        Route::get('/ubah/{kd_barang}', 'barangController@ubah');
        Route::get('/update/{kd_barang}', 'barangController@update');
        Route::get('/tambahform', 'barangController@tambahform');
        Route::get('/tambah', 'barangController@tambah');
    });


    ///////// Data kategori
    Route::prefix('kategori')->group(function() 
    {
        Route::get('/', 'kategoriController@index');
        Route::post('/update/{kd_kategori}', 'kategoriController@update');
        Route::get('/ubah/{kd_kategori}', 'kategoriController@ubah');
        Route::get('/tambahform', function() {
            return view('datakategori.tambahkategori');
            });
        Route::post('/tambah', 'kategoriController@tambah');
        Route::get('/hapus/{kd_kategori}','kategoriController@delete');
    });


    //////// Data Suplier
    Route::prefix('suplier')->group(function() 
    {
        Route::get('/', 'suplierController@index');
        Route::get('/tambah', 'suplierController@tambah');
        Route::get('/tambahform', function() {
            return view('datasuplier.tambahsuplier');
            });
        Route::get('/ubah/{kd_suplier}', 'suplierController@ubah');
        Route::get('/update/{kd_suplier}', 'suplierController@update');
        Route::get('/hapus/{kd_suplier}', 'suplierController@delete');
    });


    /////// Data pemasukan
    Route::prefix('pemasukan')->group(function() 
    {
        Route::get('/', 'pemasukanController@index');
        Route::get('/hapus/{kdtr_pemasukan}', 'pemasukanController@delete');
        Route::get('/tambahform', 'pemasukanController@tambahform');
        Route::get('/tambah', 'pemasukanController@tambah');
    });


    /////// Data pengeluaran
    Route::prefix('pengeluaran')->group(function() 
    {
        Route::get('/', 'pengeluaranController@index');
        Route::get('/hapus/{kdtr_pengeluaran}', 'pengeluaranController@delete');
        Route::get('/tambahform', 'pengeluaranController@tambahform');
        Route::get('/tambah', 'pengeluaranController@tambah');
    });


    ////// Data transaksi
    Route::prefix('transaksi')->group(function() 
    {
        Route::get('/', 'transaksiController@index');
    });

});
