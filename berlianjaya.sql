﻿# Host: 127.0.0.1  (Version 5.5.5-10.4.14-MariaDB)
# Date: 2021-07-15 14:29:57
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "databarang"
#

DROP TABLE IF EXISTS `databarang`;
CREATE TABLE `databarang` (
  `kd_barang` varchar(10) NOT NULL,
  `nama_barang` varchar(200) NOT NULL DEFAULT '',
  `kd_kategori` varchar(10) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL DEFAULT 0,
  `harga_jual` int(11) NOT NULL DEFAULT 0,
  `kd_suplier` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "databarang"
#

INSERT INTO `databarang` VALUES ('K001','Ashley 3231 Clear Wastafel','KK001',72,720000,742500,'KS001'),('k002','Heskey 3189 Red Wastafel','KK001',42,1570000,1700000,'KS001'),('K003','Nathan Series PSPVC611','KK001',13,3200000,3400000,'KS003'),('K004','Sincere Granit Lantai 60X60','KK004',100,195000,200500,'KS002'),('K005','Sincere Granit Lantai Matt','KK004',240,179000,189000,'KS002'),('K006','Sincere Granit Glazed Wood','KK004',230,179000,189000,'KS002'),('K007','Zehn Z Shape Shoe','KK002',20,330000,350000,'KS005'),('K008','Zehn Kotak Obat Stainless','KK002',14,230000,236000,'KS002'),('K009','Onda Exclusive SW','KK009',30,170000,177000,'KS004'),('K010','Ceramax Hazel DY','KK009',53,195000,200000,'KS001'),('k011','Amstad Hand Shower','KK009',21,246000,250000,'KS005'),('K012','Onda Exclusive FLS','KK007',14,220000,228000,'KS001'),('K013','Amstad IN23','KK007',13,256000,262000,'KS002'),('K014','Mushroom Trap','KK007',40,56000,60000,'KS003'),('K015','Rheem EHG 100 Liter Water Heater','KK008',5,5000000,5100000,'KS004');

#
# Structure for table "datakategori"
#

DROP TABLE IF EXISTS `datakategori`;
CREATE TABLE `datakategori` (
  `kd_kategori` varchar(10) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`kd_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "datakategori"
#

INSERT INTO `datakategori` VALUES ('KK001','Wastafel'),('KK002','Unit Rak'),('KK003','Keramik'),('KK004','Granit'),('KK005','Bathtube'),('KK006','Pintu'),('KK007','Floor Drain'),('KK008','Water Heater'),('KK009','Shower'),('KK010','Water Filter');

#
# Structure for table "datapemasukan"
#

DROP TABLE IF EXISTS `datapemasukan`;
CREATE TABLE `datapemasukan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kdtr_pemasukan` varchar(10) NOT NULL DEFAULT '',
  `tgl` date DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `user` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

#
# Data for table "datapemasukan"
#

INSERT INTO `datapemasukan` VALUES (1,'KP001','2012-12-12',500000,'aku'),(2,'KP002','2012-12-12',500000,'aku');

#
# Structure for table "datapengeluaran"
#

DROP TABLE IF EXISTS `datapengeluaran`;
CREATE TABLE `datapengeluaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kdtr_pengeluaran` varchar(10) NOT NULL DEFAULT '',
  `tgl` date DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `user` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

#
# Data for table "datapengeluaran"
#

INSERT INTO `datapengeluaran` VALUES (11,'KD001','2021-07-14',390000,'login'),(12,'KD008','2021-07-15',10000,'login'),(13,'KD009','2021-07-15',30000,'login'),(14,'KD002','2021-07-08',180000,'login');

#
# Structure for table "datasuplier"
#

DROP TABLE IF EXISTS `datasuplier`;
CREATE TABLE `datasuplier` (
  `kd_suplier` varchar(10) NOT NULL,
  `nama_suplier` varchar(30) NOT NULL,
  `nomor_tlp` varchar(15) NOT NULL,
  `alamat_suplier` varchar(50) NOT NULL,
  PRIMARY KEY (`kd_suplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "datasuplier"
#

INSERT INTO `datasuplier` VALUES ('KS001','PT Sumber Harapan','0216492938','Jl. Sukarjo Wiryopranoto No.83A, RT.8/RW.1'),('KS002','PT Arsita','02155911699','Bandara Soekarno-Hatta, Cargo Area Gedung 501, 2F'),('KS003','PT Bangun Bersama','0216003669','Plaza Harco Mangga Dua, Blok A Lt.3 No. 99'),('KS004','Brilliant Jaya','089669672614','Jl. Mujair 3 No.20, Bambu Apus, Kec. Pamulang'),('KS005','PT LMK','02138904444','Jl. Kyai Caringin No.10A');

#
# Structure for table "detailpemasukan"
#

DROP TABLE IF EXISTS `detailpemasukan`;
CREATE TABLE `detailpemasukan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `kdtr_pemasukan` varchar(11) NOT NULL DEFAULT '',
  `kd_barang` varchar(11) NOT NULL DEFAULT '',
  `kd_kategori` varchar(11) NOT NULL DEFAULT '',
  `harga` int(11) NOT NULL DEFAULT 0,
  `qty` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

#
# Data for table "detailpemasukan"
#

INSERT INTO `detailpemasukan` VALUES (1,'KD001','KD002','KD003',20000,20),(2,'1111','K020','K001',1000,110),(3,'2222','KB001','K008',50000,30),(4,'2222','KB001','K005',3000,222),(5,'2222','KB002','K004',200,222),(6,'2222','KB001','K008',222,22),(7,'2222','KB001','K008',222,222),(8,'2222','K001','KK004',1000,1);

#
# Structure for table "detailpengeluaran"
#

DROP TABLE IF EXISTS `detailpengeluaran`;
CREATE TABLE `detailpengeluaran` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `kdtr_pengeluaran` varchar(11) NOT NULL DEFAULT '',
  `kd_barang` varchar(11) NOT NULL DEFAULT '',
  `kd_kategori` varchar(11) NOT NULL DEFAULT '',
  `harga` int(11) NOT NULL DEFAULT 0,
  `qty` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

#
# Data for table "detailpengeluaran"
#

INSERT INTO `detailpengeluaran` VALUES (1,'KD001','KD001','KD002',0,2),(2,'5555','KB002','K009',5000,500),(3,'K1001','K020','K002',1000,20),(4,'K10022','K030','K003',50000,110),(5,'KP000','K020','D001',3000,20),(6,'KK000','K0','K001',20000,20),(7,'KK000','K001','K001',20000,30),(8,'KD','K004','KK004',195000,2),(9,'KD001','K006','KK004',195000,2),(10,'KD008','k002','KK002',50000,20),(11,'KD009','k002','KK001',3000,20),(12,'KD002','K005','KK004',60000,3),(13,'22','K001','KK001',50000,22);

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user` varchar(11) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `token` varchar(200) NOT NULL DEFAULT '',
  `role` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "user"
#

INSERT INTO `user` VALUES ('admin','faf9be44aca50abc4cb96d17c1795c30','0e2743c0c23e84ff1f803a07c0290ba9',0),('staff','4c96c525ce26bd09109b7978182544ec','',1);
