
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
          <a href="/">
            <i class="nc-icon nc-shop"></i>
            <p>Dashboard</p>
          </a>
        </li><hr>
        <li>
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li class="active ">
          <a href="javascript:;">
            <i class="nc-icon nc-diamond"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li>
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        @if (Session::get('role') == 0)
        <li>
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        @endif
        <li>
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Data Ketegori</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
        <div class="row">
          <div class="col-md-12">
            
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show">
              <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                <i class="nc-icon nc-simple-remove"></i>
              </button>
              <span><b> {{Session::get('status')}} - </b>{{Session::get('message')}}</span>
            </div>
            @endif

            <div>
              <a id="btn-tambah" href="/kategori/tambahform" class="btn btn-md shadow rounded bg-info" style="border-radius:25px;">+ Tambah Kategori</a>
              </div><hr>
            <div class="limiter">
              <div class="container-table100" style="width:100%; background:none;">
                
                <div class="wrap-table100">
                  <div class="table100 ver1 m-b-110">
                    <div class="table100-head">
                      <table>
                        <thead >
                          <tr class="row100 head">
                            <th class="cell100 column1">Kode Ketegori</th>
                            <th class="cell100 column2">Nama Kategori</th>
                            <th class="cell100 column5">Pilihan</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
          
                    
                    <div class="table100-body js-pscroll">
                      <table>
                        <tbody>
                          @foreach ($datakategori as $kategori)
                          <tr class="row100 body">
                            <td class="cell100 column1">{{ $kategori['kd_kategori'] }}</td>
                            <td class="cell100 column2">{{ $kategori['nama_kategori'] }}</td>
                            <td class="cell100 column5">
                              <a href="/kategori/ubah/{{$kategori['kd_kategori']}}" class="nc-icon nc-settings">
                                Ubah
                            </a>
                            <a href="/kategori/hapus/{{$kategori['kd_kategori']}}" class="nc-icon nc-simple-remove" >
                                Hapus
                            </a>
                            </td>
                          </tr>
                          @endforeach
          
                        </tbody>
                      </table>
                    </div>
                  </div>


          </div>
        </div>
    </div>

@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>