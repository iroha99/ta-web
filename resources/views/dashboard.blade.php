
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="active ">
            <a href="javascript:;">
              <i class="nc-icon nc-shop"></i>
              <p>Dashboard</p>
            </a>
          </li><hr>
        <li>
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li>
          <a href="/kategori">
            <i class="nc-icon nc-diamond"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li>
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        @if (Session::get('role') == 0)
        <li>
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        @endif
        <li>
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Dashboard</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
      <div class="container" style="background-color: white; padding-top:10px">
        <div class="row">
          <div class="col-lg-12" style="padding-bottom:10px">
            @if (Session::get('role') == 0)
                <h3>Selamat datang, Admin</h3>
            @else
                <h3>Selamat datang, Staff</h3>
            @endif
          </div>
        </div>
      </div>
      <br>

      <div class="container" style="background-color: white; padding-top:10px">
        <div class="row">

          <div class="col-lg-4">
            <div class="card text-white" style="text-align:center; background-color:#576490">
                <div class="card-header">
                    <p>Total Stok Barang</p><hr>
                </div>
                <div class="card-body" style="height:70px; padding-top:0px">
                    <h2>{{$stok}}</h2><br>
                </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="card text-white" style="text-align:center; background-color:#576490">
                <div class="card-header">
                    <p>Total Data Kategori</p><hr>
                </div>
                <div class="card-body" style="height:70px; padding-top:0px">
                    <h2>{{$kategori}}</h2><br>
                </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="card text-white" style="text-align:center; background-color:#576490;">
                <div class="card-header">
                    <p>Total Data Suplier</p><hr>
                </div>
                <div class="card-body" style="height:70px; padding-top:0px">
                    <h2>{{$suplier}}</h2><br>
                </div>
            </div>
          </div>

        </div>
        
        <div class="row">
            <div class="col-lg-6">
              <div class="card text-white" style="text-align:center; background-color:#3E850D">
                  <div class="card-header">
                      <p>Total Pemasukan / Penjualan</p><hr>
                  </div>
                  <div class="card-body" style="height:70px; padding-top:0px">
                      <h2>{{rupiah($pemasukan)}}</h2><br>
                  </div>
              </div>
            </div>
  
            <div class="col-lg-6">
              <div class="card text-white" style="text-align:center; background-color:#B71919;">
                  <div class="card-header">
                      <p>Total Pengeluaran / Pembelian</p><hr>
                  </div>
                  <div class="card-body" style="height:70px; padding-top:0px">
                      <h2>{{rupiah($pengeluaran)}}</h2><br>
                  </div>
              </div>
            </div>
        </div>

      </div><br>

      <div class="container" style="background-color: white; padding-top:10px">
        <div class="row">
          <div class="col-lg-12">
            <div class="card text-white" style="text-align:center; background-color:#DBAC31">
                <div class="card-header">
                    <p>Total Balance</p><hr>
                </div>
                <div class="card-body" style="height:70px; padding-top:0px">
                    <h2>{{rupiah($balance)}}</h2><br>
                </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    

@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>