
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
            <a href="/">
              <i class="nc-icon nc-shop"></i>
              <p>Dashboard</p>
            </a>
          </li><hr>
        <li class="active ">
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li>
          <a href="/kategori">
            <i class="nc-icon nc-diamond"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li>
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        @if (Session::get('role') == 0)
        <li>
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        @endif
        <li>
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Ubah Data Barang</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
        <div class="row">
          <div class="col-md-12">

            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show">
              <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                <i class="nc-icon nc-simple-remove"></i>
              </button>
              <span><b> {{Session::get('status')}} - </b>{{Session::get('message')}}</span>
            </div>
            @endif
            
    <div class="card bg-danger" style="width: 40%">
        <div class="card-body">
          @foreach ($databarang as $d)
            <form action="/barang/update/{{$d['kd_barang']}}">
                <div class="form-row align-items-center">
                <div class="form-group" style="width: 100%">
                  <label for="namabarang" style="color: black">Nama Barang</label>
                  <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="" value="{{$d['nama_barang']}}">
                </div>
                <div class="form-group" style="width: 100%">
                  <label for="kategoribarang" style="color: black">Kategori Barang</label>
                  <select class="custom-select mr-sm-2" name="kd_kategori" id="inlineFormCustomSelect">
                    @foreach ($databarang as $b)
                      <option value="{{$b['kd_kategori']}}" selected>{{$b['kd_kategori']}}</option>
                      @endforeach
                  </select>
              </div>
                <div class="col-12 my-1">
                <div class="form-group" style="width: 20%">
                    <label for="stokbarang" style="color: black">Stok</label>
                    <input type="text" class="form-control" name="stok" value="{{$d['stok']}}">
                </div>
                </div>
                <div class="col-6 my-1">
                    <div class="form-group" style="width: 100%">
                        <label for="hargabeli" style="color: black">Harga Beli</label>
                        <input type="text" class="form-control" name="harga_beli" placeholder="" value="{{$d['harga_beli']}}">
                    </div>
                </div> 
                <div class="col-6 my-1">
                    <div class="form-group" style="width: 100%">
                        <label for="hargajual" style="color: black">Harga Jual</label>
                        <input type="text" class="form-control" name="harga_jual" placeholder="" value="{{$d['harga_jual']}}">
                    </div>
                </div>
                <div class="form-group" style="width: 100%">
                  <label for="suplier" style="color: black">Suplier</label>
                  <select class="custom-select mr-sm-2" name="kd_suplier" id="inlineFormCustomSelect">
                      <option value="{{$d['kd_suplier']}}" selected>{{$d['kd_suplier']}}</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                  </select>
              </div>
                <button class="btn btn-primary" type="submit">Ubah</button>
                {{-- <div class="form-group">
                  <label for="exampleFormControlTextarea1">Example textarea</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div> --}}
                </div>
                @endforeach
              </form>
        </div>
    </div>

          </div>
        </div>
    </div>

    

@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>