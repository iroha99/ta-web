
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
          <a href="/">
            <i class="nc-icon nc-shop"></i>
            <p>Dashboard</p>
          </a>
        </li><hr>
        <li>
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li>
          <a href="/kategori">
            <i class="nc-icon nc-tag-content"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li>
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        @if (Session::get('role') == 0)
        <li>
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        @endif
        <li  class="active ">
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Data Transaksi</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
        <div class="row">
          <div class="col-md-12">
            
            
<br>
            <div class="limiter">
              <div class="container-table100" style="width:100%; background:none;">
                
                <div class="wrap-table100">
                  <div class="table100 ver1 m-b-110">
                    <div class="table100-head">
                      <table>
                        <thead >
                          <tr class="row100 head">
                            <th class="cell100 column1">Kode</th>
                            <th class="cell100 column2">Tanggal</th>
                            <th class="cell100 column3">Pengeluaran</th>
                            <th class="cell100 column4">Pemasukan</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
          
                    <div class="table100-body js-pscroll">
                      <table>
                        <tbody>
                          @foreach ($transaksi as $d)
                          <tr class="row100 body">
                            <td class="cell100 column1">{{$d['kode']}}</td>
                            <td class="cell100 column2">{{$d['tgl']}}</td>
                            @if(\Illuminate\Support\Str::limit($d['kode'], $limit = 2, $end = '') == 'KD')
                            <td class="cell100 column3">{{ rupiah($d['subtotal']) }}</td>
                            <td class="cell100 column4">-</td>
                            @else
                            <td class="cell100 column3">-</td>
                            <td class="cell100 column4">{{ rupiah($d['subtotal']) }}</td>
                            @endif
                          </tr>
                          @endforeach
                          <tr style="background-color: rgb(194, 140, 79)" class="row100 body">
                            <td style="color:white" class="cell100 column1" colspan="2"><b>JUMLAH</b></td>
                            <td style="color:white">{{$total_pengeluaran}}</td>
                            <td style="color:white">{{$total_pemasukan}}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>


          </div>
        </div>
    </div>

   

@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>