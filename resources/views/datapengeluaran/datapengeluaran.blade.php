
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
          <a href="/">
            <i class="nc-icon nc-shop"></i>
            <p>Dashboard</p>
          </a>
        </li><hr>
        <li>
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li>
          <a href="/kategori">
            <i class="nc-icon nc-tag-content"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li>
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        <li class="active ">
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        <li>
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Data Pengeluaran</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
        <div class="row">
          <div class="col-md-12">
            
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show">
              <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                <i class="nc-icon nc-simple-remove"></i>
              </button>
              <span><b> {{Session::get('status')}} - </b>{{Session::get('message')}}</span>
            </div>
            @endif

            <div>
              <a id="btn-tambah" href="/pengeluaran/tambahform" class="btn btn-md shadow rounded bg-info" style="border-radius:25px;">+ Tambah Data</a>
              </div> <hr>
            <div class="limiter">
              <div class="container-table100" style="width:100%; background:none;">
                
                <div class="wrap-table100">
                  <div class="table100 ver1 m-b-110">
                    <div class="table100-head">
                      <table>
                        <thead >
                          <tr class="row100 head">
                            <th class="cell100 column1">Kode</th>
                            <th class="cell100 column2">Tanggal</th>
                            <th class="cell100 column3">Subtotal</th>
                            <th class="cell100 column4">User</th>
                            <th class="cell100 column5">Pilihan</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
          
                    <div class="table100-body js-pscroll">
                      <table>
                        
                        <tbody>
                          @foreach ($datapengeluaran as $p)
                          
                          <tr class="row100 body">
                            <td class="cell100 column1">{{$p['kdtr_pengeluaran']}}</td>
                            <td class="cell100 column2">{{$p['tgl']}}</td>
                            <td class="cell100 column3">{{ rupiah($p['subtotal']) }}</td>
                            <td class="cell100 column4">{{$p['user']}}</td>
                            <td class="cell100 column5">                        
                              <a href="/pengeluaran/hapus/{{$p['kdtr_pengeluaran']}}" class="nc-icon nc-simple-remove" >
                                  Hapus
                              </a>
                            </td>
                          </tr>
                          @endforeach
                          <tr style="background-color: rgb(194, 140, 79)" class="row100 body">
                            <td style="color:white" class="cell100 column1" colspan="1"><b>JUMLAH</b></td>
                            <td style="color:white"></td>
                            <td style="color:white">{{$total_pengeluaran}}</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>


          </div>
        </div>
    </div>

    <div class="modal fade" id="modalinfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Detail Transaksi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            

              @foreach ($detailpengeluaran as $d)
              <p>kode transaksi: {{$d['kdtr_pengeluaran']}}</p><br>
              <p>kode barang: {{$d['kd_barang']}}</p><br>
              <p>kode kategori: {{$d['kd_kategori']}}</p><br>
              <p>harga: {{$d['harga']}}</p><br>
              <p>Quantity: {{$d['qty']}}</p><br>
              @endforeach

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>