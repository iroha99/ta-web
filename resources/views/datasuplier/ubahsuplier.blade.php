
@include('layouts.header')

{{-- @include('layouts.navbar') --}}
<div class="wrapper ">
  <div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
      <a href="#" class="simple-text logo-mini">
        
      </a>
      <a href="#" class="simple-text logo-normal">
        Berlian Jaya
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li>
            <a href="/">
              <i class="nc-icon nc-shop"></i>
              <p>Dashboard</p>
            </a>
          </li><hr>
        <li>
          <a href="/barang">
            <i class="nc-icon nc-bank"></i>
            <p>Data Barang</p>
          </a>
        </li>
        <li >
          <a href="/kategori">
            <i class="nc-icon nc-diamond"></i>
            <p>Data Kategori</p>
          </a>
        </li>
        <li class="active ">
          <a href="/suplier">
            <i class="nc-icon nc-pin-3"></i>
            <p>Data Suplier</p>
          </a>
        </li><hr>
        @if (Session::get('role') == 0)
        <li>
          <a href="/pengeluaran">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pengeluaran</p>
          </a>
        </li>
        <li>
          <a href="/pemasukan">
            <i class="nc-icon nc-money-coins"></i>
            <p>Data Pemasukan</p>
          </a>
        </li>
        @endif
        <li>
          <a href="/transaksi">
            <i class="nc-icon nc-money-coins"></i>
            <p>Semua Transaksi</p>
          </a>
        </li>
        <li>
          <a href="/logout">
            <i class="nc-icon nc-user-run"></i>
            <p>Logout</p>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="main-panel" style="height: 100vh;">


  <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
      <div class="container-fluid">
        <div class="navbar-wrapper">
          <div class="navbar-toggle">
            <button type="button" class="navbar-toggler">
              <span class="navbar-toggler-bar bar1"></span>
              <span class="navbar-toggler-bar bar2"></span>
              <span class="navbar-toggler-bar bar3"></span>
            </button>
          </div>
          <a class="navbar-brand" href="javascript:;">Ubah Data Suplier</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
          <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        
      </div>
    </nav>

    <div class="content">
        <div class="row">
          <div class="col-md-12">

            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show">
              <button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Close">
                <i class="nc-icon nc-simple-remove"></i>
              </button>
              <span><b> {{Session::get('status')}} - </b>{{Session::get('message')}}</span>
            </div>
            @endif
            
    <div class="card bg-danger" style="width: 40%">
        <div class="card-body">
          @foreach ($datasuplier as $d)
            <form action="/suplier/update/{{$d['kd_suplier']}}">              
                <div class="form-group" style="width: 100%">
                  <label for="namasuplier" style="color: black">Nama Suplier</label>
                  <input type="text" class="form-control" name="nama_suplier" id="nama_Suplier" placeholder="" value="{{$d['nama_suplier']}}">
                </div>
                <div class="form-group" style="width: 100%">
                  <label for="nosuplier" style="color: black">No tlp. Suplier</label>
                  <input type="text" class="form-control" name="nomor_tlp" id="nomor_tlp" placeholder="" value="{{$d['nomor_tlp']}}">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1" style="color: black">Alamat Suplier</label>
                    <textarea class="form-control" name="alamat_suplier" id="alamat_suplier" rows="3" placeholder="">{{$d['alamat_suplier']}}</textarea>
                </div>
                <button class="btn btn-primary" type="submit">Ubah</button>
                @endforeach
              </form>
        </div>
    </div>

          </div>
        </div>
    </div>



@include('layouts.footer')

<!--   Core JS Files   -->
  <script src="{{ url('js/core/jquery.min.js') }}"></script>
  <script src="{{ url('js/core/popper.min.js') }}"></script>
  <script src="{{ url('js/core/bootstrap.min.js') }}"></script>
  <script src="{{ url('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{ url('js/plugins/chartjs.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>

	<script src="{{ url('vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ url('vendor/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
	<script>
		$('.js-pscroll').each(function(){
			var ps = new PerfectScrollbar(this);

			$(window).on('resize', function(){
				ps.update();
			})
		});
			
		
	</script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>